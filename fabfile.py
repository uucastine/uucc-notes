from fabric.api import *
from boto.s3.connection import S3Connection
import os
from boto.s3.key import Key

ignores = [".git", "venv"]

AWS_KEY = os.environ['AWS_KEY']
AWS_SECRET = os.environ['AWS_SECRET']


def deploy(domain, folder):
    conn = S3Connection(AWS_KEY, AWS_SECRET)
    domain_arr = [domain]

    for d in domain_arr:
        bucket = conn.get_bucket(d)
        print bucket
        for root, dirs, files in os.walk(folder):
            for name in files:
                path = root.split(os.path.sep)[1:]
                path.append(name)
                path.remove('html')
                key_id = os.path.join(*path)
                k = Key(bucket)
                k.key = key_id
                k.set_contents_from_filename(os.path.join(root, name))
                k.make_public()
                print 'Copying: ' + key_id
