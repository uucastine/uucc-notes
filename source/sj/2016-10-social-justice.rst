========================================
October 2016 UUCC Social Justice Meeting
========================================

UUCC Social Justice Committee
Meeting of October 18, 2016

---------------
Members present
---------------

* Colin Powell, Chair
* Anne Price
* Lynn Parsons
* Jim Schatz
* Chris Michaels
* Anne Romans
* Margaret Beckman

-------
Minutes
-------

Meeting called to order at 8:40am

Discussion ensued, covering a variety of topics, not necessarily in this order.

Next year’s Pulliam Grant budget was announced as $36,000. [Subsequent information has reduced it to $30,000 – LHP]

We will be promoting social justice and the ways in which the three-church collaboration can promote it at one of the sessions at the Ferry Beach retreat.  Parsons and Beckman to organize and lead this session.

We should explore the possibility of providing funds for operating expenses for applicants, as well as long-term, multi-year funding.  Also the possibilities of “mini-grants” 

We need better publicity for our efforts. 

Congregations could be polled for their input.

There will be a follow-up meeting on November 4 at Ellsworth.  Time TBD.

Meeting adjourned, 9:32am.


-------------
Colin's Notes 
-------------

Need a way to take temperature of our congregations, on SJ issues.

What is the nature of our relationship with other congregations?

We need to spread the enthusiasm.

The universe we've been dealing with is small.

What should be the themes of the collective groups?

One idea from Chirs is to hold an event to get temperature of our congregations That event kicks off next step, collaboration of utilization of resources.
We need a process that leads to a new grant program. One idea from Chris was to crowdsource outreach ideas to help expand our issues we're addressing.

Give one person from each cong. repsonsibility to meet within their congregation to get temperature coming out of Ferry Beach, hosting conversations. Follow up with a SJ desert social, allow folks to be the speakers. Perhaps hold it in a neutral location like Bucksport?

Ferry Beach will be two things: 

1. People who show up get an introduction to the Pulliam Grant platform, we are asking them to come on board. 
2. How reps from our three congregations envisoin their local efforts at SJ

What wiil the collaboration look like? Driving time is a major factor in our ability to collaborate. Need to consider telecommuting.

Put up questions that we have now, as point for folks getting inovlved.
