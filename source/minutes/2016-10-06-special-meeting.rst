==============================================
Congregational meeting October 9, 2016 minutes
==============================================

President Lynn Parsons opened the meeting at 11:32 a.m on Sunday, October 9,
2016.

A quorum was declared with 13 members present.

Lynn noted that Article 5 of the proposed a handful of minor changes to language
in Articles 4, 6, and 7.

Motion made to adopt changes as presented by Sue Clement, Anne Price seconded
the motion. Diana Bernard asked for clarification on the budget language
changes.

The motion passed unanimously.

Lynn read through all proposed Articles in the new by laws, pausing for
comments, questions and amendments.

On Article 7, Standish Bourne noted that the chairman of the finance committee
and treasurer should be two different people.

Stan moved to amend Article 7 to read: "The chairman of the finance committee shall be someone other than the
treasurer." Diana Bernard seconded the motion.

After no discussion, the motion was approved unanimously.

Stan moved to amend the nominating committee section to include "No member of
the governing board shall be a member of the nominating committee" Brooke
seconded the motion.

After brief discussion, Stan withdrew the motion and Brooke accepted the withdrawl.

Lynn Parsons moved to amend Article 9 to include the language "shall be
ex-officio member of all committees." Anne Price seconded the motion.

The motion was approved unanimously.

Gil Tenney moved to approve the entire document as amended. Kent Price seconded
the motion.

The motion was approved unanimously.

Brooke Tenney thanked Lynn for his work on the new by laws.

Anne Price moved to adjourn. Anne Parsons seconded. The motion was approved
unanimously.

The meeting was adjourned at 11:59 a.m.
