==============================
General Assembly 2016 Business
==============================

---------------------
Social Witness Issues
---------------------

There are currently four proposals, three will be presented for selection.

  * Climate change
  * Race
  * Gun violence
  * Corruption of democracy


-----------------------
Commission on Appraisal
-----------------------

Nato Hollister

Study on class and classism. The commission, reaffirmed at GA 2015. Commission has gone from 2 to 6 people. Recommend no new commissioners be nominated. Focusing on how classim affects each of differently. Collecting stories, find a link on uua.org. Report on class in UUA will be presented next year.


---------------------------------
Congregtaional Study/Action Issue
---------------------------------

Text found in program books.

  * CSAI 1 - Climate change & enviro. justice
  * CSAI 2 - A national conversation on race
  * CSAI 3 - Ending gun violence in America
  * CSAI 4 - The corruption of our democracy

After a too close to call run off between CSAI 2 & 4, votes were hand counted.

**CSAI 4 selected as next initative**

The full text follows.

`Read CSAI 4 on the UUA website <http://www.uua.org/statements/current/proposed-congregational-study/action-issue-corruption-our-democracy/>`_

**Issue:** As corporations use “corporate personhood” and “money as speech” for their interests rather than the people’s needs, can a constitutional amendment be passed to protect us? Could this further Unitarian Universalist work for social and environmental justice and help protect the health and safety of the people and the planet?

**Grounding in Unitarian Universalism:** Our Seven Principles give us spiritual grounding to put our faith into action. Our General Assembly considered CSAI proposals in 2010 and 2014 on “Revitalizing Democracy” and passed AIWs in 2011 and 2013 on amending the constitution. It is time for a full study of a possible 28th amendment.

Topics for Congregational Study
===============================

Unitarian Universalists have identified escalating inequality, racial justice, voting rights, immigration, reproductive justice, marriage equality and more as major moral concerns. Now climate change looms large.

Could passing an amendment establishing that only human beings, not corporations, unions and other artificial entities, have constitutional rights, and money is not free speech, benefit these important issues?

What does “corporate personhood” mean and why is it important to address that in addition to “money as speech” in a proposed constitutional amendment?

How have Supreme Court decisions over the past two hundred years created this “legal fiction”?

How does treating corporations and other artificial entities as persons violate our Unitarian Universalist principles?

Is a moral political revolution needed to address voting rights, gerrymandering, voting methods, and the possibility of public financing of campaigns? 

Possible Congregational/District Actions
========================================

Hold forums in your congregation on the various proposals to overturn Citizens United v. FEC and other Supreme Court decisions giving moneyed interests sway in this country.

Show the 30-minute film “Legalize Democracy” and discuss how this supports our Seven Principles and affects all ages.

Encourage your congregation to pass a resolution for Move to Amend and endorse it.

Encourage your City Council and County Commission to pass a resolution for Move to Amend, and work to place a ballot initiative or non-binding referendum on your city or county ballot, so all voters can speak out on this important issue. Invite youth to participate in this effort.

Educate your congregation about the Interfaith Caucus of Move to Amend. Use the issue of climate change to reach out to other faiths.

Join your UU State Action Network, or start one if your state does not yet have one, and encourage them to include climate change as a core issue and the amendment movement to help address it.







