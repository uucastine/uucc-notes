General Assembly 2016 Workshops
===============================

Legacy or Renaissance? Small Congregations on the Edge
------------------------------------------------------

20th century model of church in many UU congregations. 21st century church looks different. Think about how we might change our structure to reflect new realities.

People are very attached to church homes. Change is hard.

  * Church committment is different: 1950s cultural model driven largely by women
  * Church membership is different
  * Stewardship models are different; pledge doesn't work the way it used to
  * New models of religious education; more interested in mutli-generational actions
  * Different target audience; largely disaffected Christians, new gen. that have been un churched
  * New cultural landscape; Sundays are a very segregated time, must figure out how to be inclusive
  * Do we have to live tweet everything now? How to integrate technology into Sundays


Is your congregation attendence flat, declining or gone? Have you been disappointed by stewardship campaign? Are you using your endowment to cover operating expenses? Are you considering reducing budget by cutting hours of minister or staff? Are the same volunteers doing everything that they have been the same people? Are you drained?

You are not alone. Trend we're seeing through congregational life staff. It is endemic. Seeing it in New England and all around the country. How do we inject energy into our congregations?

Things to remember
~~~~~~~~~~~~~~~~~~

  * You aren't alone
  * You're great at some things (and not great at others)
  * You can't do everything well (even though that's what we think we're supposed to)

UUA has given a model for what a full-service church is supposed to be. Full time staff paid for by membership, your own building, service every Sunday, and change the world. Spent a lot of time feeling ashamed as a congregation.

Rather than full-service, now focused on finding a narrow mission. Work with neighbors to get things done together.
  
Is it time to ask what's next? That means leaving some things behind. Offer us permissions to leave what you think church is behind and start something new. The goal is increased energy and joy.

Time for Honesty
~~~~~~~~~~~~~~~~

  * What is life giving at your congregation? Be honest.
  * What is draining, demoralizing or unsustainable?

Memories, Hopes and conversations by Mark Lau Branson

Practice detachment when it comes to outcomes. Maybe the congregation as it has been has run it's course and it's time for something new. Be curious, look for the joy and increased energey. Once you learn to see it, it's not hard to see.

Find and encourage the "experi-fail" and make it a new part of your congregational culture. Fast failing, make it okay, you're looking for joy.

Become a learning community. Learn about your environment, what's changing, what are the needs of your community? The work you're called to do is where our own hunger and your community intersect? What needs exist in your community exist that you are unaware of? Most challenging aspect is being honest about where your hunger intersects the community. You have to need each other.

We can make our community a bastion of support, love and assistance if we want to be.

Deal with your unproductive conflicts. "So and so, wont allow this change to happen ..." is a poison. RE, choir, old guard, wont allow change? Poor communication habits? Good time to reach out to your congregational life staff. It can be resolved. Make a plan to deal with it. Have disernment conversations.

Rev. Mary Grigolia, North Olmstead

Started second-half of life reflection groups and discovered that those folks started to bring their friends, and they brought their grandchildren. Offering a totally different model for what membership means. Small congregations are very susceptible to intra-personal conflict. Congregations are full of people who are not skilled in conflict management.

Controversy, at North Olmsted become deeply personalized. Every time we brought in the district staff, thought it would make it better, it only made it worse. Had people who would not step into the river. Held a workshop (Kat Cox) to give us all the same vocabulary. The experience taught that no matter the books, or processes we implement, this is emotional material and we have to learn how to be present with one another with great kindness. 

  * Be who you are
  * Work together to find the joy
  * Follow your energy
  * Be curious
  * Work with your neighbors!
  * Get help from congregational life staff


360 Leadership System
---------------------

A leadership development program for congregations, offered using technology. 

All churches want to grow in:

  * Members
  * Youth
  * Diversity
  * Impact
  * Financial stability

Itzak Izidtis model. Every congregation has an organic lifecycle: born, thrive, die.

Good to know where you are in the cycle. Leaders need to adapt. Need to know where you are in your lifecycle in order to know what to expect:

Courtship -> Infant -> Go-Go -> Adolescence -> PRIME -> The Fall -> Aristocracy -> Recrimination -> Bureaucracy -> Death

Courtship to PRIME is growing.

The Fall to Recrimination is Aging

Bureaucracy to Death is Dying

Ministries at adolecence can fall into a founder's trap, a senior minister who's vision is the law, or the only source of leadership. Can also lead to splitting into camps where that vision doesn't exist, leads to Divorce in the PRIME stage.

We have never had a UU church in PRIME!

Biggest historical churches were built on charisma and did not have a system to encourage and build programs.

  * Your congregation is prefectly designed to get the results you're currently getting.
  * Prime is the place to be, hard to get there, harder to stay there
  * Having problesm is normal and desirable, the trick is to have the right problems at the right stage
  * All aging is abnormal, but in orgs, it can be prevented and reversed


Leadership is managing problems towards opportunity. Overcome one problem after another. Make sure you have the right problems. There are pathological problems, peeing the bed is okay if you're 18 months old, it's not okay if you're 43. What gets you to one level might not get you to the next level. Leadership change is okay, so long as it's done with the right problem solving method in mind. You don't need huge energy and entrepenurship to systamitize things. Organizations in PRIME build leadership and provides services and education.

Power sharing is a huge part of effective leadership and moving between levels. 

  * How does learning about lifecycle and leadership apply to your congregation?
  * What could you do with this information back home?

Life stage diagnosis is by nature subjective, but it should be intential, consistent and re-evaluated periodically. You can be in multiple life stages through out your congregation. Decision making process ... who makes decisions, who's included in discussions, how are they communicated?

Do you announce it to an email list? Do you phone one or two people? 

5 to 10 PRIME congregations in UU would change the landscape.

Fahs Lecture
------------

Protestant Americans are a minority, US is a majority religious minority country. Perhaps the first time this has happened in history. What does this look like? Anger, rage at losing power. Fear, obliviousness. What does this look like for the rising minorities (LGBTQ, liberal theologies)? Celebration, fear, anxiety, rage in reverse.

Understand how to read responses. Kim Davis ... sad state. Easy to be angry, but her response is grief. Grief of losing power. Same with many traditionally evangelical understandings of Christianities. Not one Christianity, many, and discovering now they all interpret things differently.

Approach the grief and anger from a perspective of understanding. Liberal theology has been a minority. Resist urge to rise up and be either inferior or superior. Engage intelletually where possible, and disarm with understanding and compassion in the face of anger and grief. Ask a lot of clairfying questions. Romans 1:27 ... sure, it can be used to prop up homosexual relations, but what to of the next passage that demands those participating be put to death? How to understand hate directed towards homosexuals in the name of a loving Christ? Present these questions, and step aside and allow the new minorities to grieve properly.

Ground our faith education in legal, religious liberty.

The Williamsburg Charter

Outreach 101
------------

Outreach is talking to people with ideas different from ours.

Used to be a decision of this church or that church ... we were the alternative then. Now it's church or family/personal time. We are not an alternative to that. We are also a faith of converts

On the street level
~~~~~~~~~~~~~~~~~~~

You cannot build relationships in general, must be specific to need, values. Must build empathy and deep relationships around things that matter to folks. You have to need each other.

How to figure who you're building relationships with? You have to approach a group of people. Authentic ministry and growth opportunity meets in the middle with your target audience. Overlap between your energy and community energy is the sweet spot. Start with what you have and where people are and do outreach there.

Findable. Accessible. Usable.

You have to put on your empathy goggles.  What groups do you most want to work with? 5 people you can call, or email to have a conversation with to ask how they perceive your congregation.

**I can't know what you think, unless I ask.**

UU brand idenity. UUA has a logo. But that's only one little piece. At assoc. level, been trying to create a look and feel that will resonate with people. Bright colors, people, easing up on text.

How do we start?
~~~~~~~~~~~~~~~~

Join our club? --> Join our **cause**!

There's good reasearch that people do no seek out religious community for friends, they want to find meaning, purpose or are struggling. Need an anchor point. They stay because they make friends. It's not the reason people show up in the first place. "All are welcome," is not true. WBC folks are not welcome here if they don't let up on their hate-based value system.

Anyone can go to the mall. All are welcome there. We are looking for people who share our value and want to participate. Women's spirit circle, meeting every week for the last 6 years are not the easiest entry point. Sunday services are not. Services have to do a whole lot of heavy lifting to setup. Consider creating bite-sized missions.  Does it pass the "friend" test. Would you bring your friend and feel comfortable leaving them alone?

Play GroUUp --> UU of LV has 

Tiney house event --> Redwood City, CA

Affortable housing forum --> Fremont, CA

Luna Rising --> Charlotte, NC

Full Moon Rising --> Davie, FL, outdoor outreach, lots of non-UUs

Seedy Saturdays --> Missassuga, ON

Little Helpers --> Cordova, TN

How to talk about sex (for parents) --> Washington Ethical Society

Brainstorm five ideas for outreach. How do you connect those outreach entry events to your mission?

First thing anyone does when they hear about your congregation is Google it. Do at least:

  * Have a presence on social media and your website. Website should be decent visually, accessible, and responsive.
  * Have an up to date Facebook page. 
  * Check your reviews and search results


Tools:
  
  * #AllUU promo tool kit coming in the fall.
  * UUA Wordpress theme
  * Meetup.com
  * Mass incarceration as a moral issue event


Unique community issues. Small town, end of the road. Means holding your events not at your church as often. Get out into the communities where people are. Build a following amongst folks who may not come on Sunday, but who network and KNOW what UUCC is all about.

Capacity is the single greatest challenge for a congregation. How to do outreach given the reality of capacity (largely volunteer-powered), focus on what you can do. Pick a niche, focus on that, create a relatively small, solid team. Start with something that you can do. Can you show up for one inter-faith action event? One workshop? Own the event.

Standing on the side of love is not UU-first. Not clear it's a UUA program. Lead with the cause, and then connect it to the UU faith tradition. Not always easy to make the final ask or push about UU. Community example had a big community meal and never opened the discussion about the UU underpinnings. Don't lead with UU in the way you title or promote your event. But at some point, someone should stand up and explain that the event is UU powered. "This event was sponsored by UUCC, because we believe in the inherent worth and dignity of all people, and that extends to those struggling with poverty."

UUA has been building better tools for outreach for you. Consider using the UUA Wordpress template and Emma or Mailchimp for email sending.

4X4 Plan

  * Hold 4 outreach events each year
  * Develop 4 pieces of original content to share on social media


Clusters and Partnerships
--------------------------

"... alone our vision is too narrow. Together our strength is renewed."

Collaboration & partnership is a **mindset** not just a model.

How close the congregations are together, changes what works in an area, as do cultures, regions, individuals. Looking at our congregations as interdependent, rather than independent or dependent. 

Four kinds of networks
~~~~~~~~~~~~~~~~~~~~~~

  * Learning networks
  * Doing networks
  * Sharing networks
  * Being networks

In learning networks, we are coming together as congos to do workshops together, or learning events. Joining with regional. UU Leadership Institute is an example. Fixes issues of learning being limited in these scenarios to the experience of your congregations. Shares stories and deepens the learning exp.

In doing networks, co-hosting concert, outreach event, or to accomplish something

Sharing networks pool resources to do together what you couldn't do alone.

Being networks merge our congregationsto see ourselves as UUs together, rather than separate congregations.

Clusters: The manifestations of relationships between congregations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  * May be UU, but do not have to be
  * May form and reform around an issue or an event
  * May be a formal org or may be informal group of cross-congregational members and leaders
  * May be a cluster of ministers or RE or SJ committees
  * Focused on shared understanding, coordination, collaboration between congregations

Why clusters? They are a visible, experienced manifestation of a belief in interdependence. Even our largest congregations, compared to the rest of society, they are little pockets of UUs. Our power comes in our connectoins. Our power comes from our interdependence. Live that theology of interdependence. Coordinating with other UU congos is the low hanging fruit of interdependence. That's our first step.

Next, the world needs an organized movement of UUs beyond any one congregation. We are limiting our effect by not seeking out connections actively. 

We bring bigger events together. We can amplify our message together.

Great to have visibility into what other congregations are doing, and allow us to learn together. Each congregation can offer something different on different days.

It means we know we're not alone. Being UU can be an isolating experience. Power in knowing there are other UUs near you, even if it's a drive.

Best practices
~~~~~~~~~~~~~~

  * Meet on a regular basis, schedule an event far in advance, set a regular schedule
  * Meet both in person and by teleconference
  * Form a close relationship between the cluster and the UUA regional staff who serve the congos
  * Have cluster reps formally chosen by and accountable to congo boards
  * Start with what is possible, even if it's small
  * Remain in accountable relationships with congo ministers
  * Do stuff together!


