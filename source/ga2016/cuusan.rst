CUUSAN Mini Conference @ GA 2016
================================


Keynote for CUUSAN: Kathleen McTigue
-------------------------------------

Director of UU College of Social Justice

Our faith calls us to a spiritual honesty. We should trust in our abilities to translate one another's vocabulary

Cumulative effects of:

If we give up our religious vocabulary, we sacrifice our only means to be a community. We shrink our capacity to move gracefully through the world out there. With more shades of diversity. Our strength is our diversity.

Will ours turn out to be an inarticulate mumble? When any demon. considers questions of their own existence, we UUs should be able to answer:

What is it that UU proclaims, without which, the expression of faith in the world would not be whole? What does our faith teach, that we take to be precious and essential for the wholeness of the world?

McTigues's:

Profound interdependence with all people, with all beings and with the Earth itself; Gustavo Gutierrez ... the first act of human violence, in Genesis 4:8, Where are you? Where is your brother? Where is your sister? Liberal faith compels us to ask that in the collective voice: Where are we? Tangle of privilege and power, where are we? Attending to those questions is not easy or comfortable. Asking them profoundly can be discomforting.

Faith teaches us that our boundries are delusions errected by fear, habit or ignorance When we find courage to step beyond them, the place where we can feel at home grows larger. So then do our potential community. Changing ourselves is linked inextricably to changing our world
  
At advocacy group planned by UU

A reporter from CNN singles you out, stick mic in your face: "Why are you here today, in one sentence?"

"I'm here to give voice to the voiceless who could not, or will not be here."

Does that statement give grounding to commanded by the convications of your faith? Anything that alludes to our core values of our UU principles?

Try again, using language of our faith.

"I am commanded by love and respect for the sanctity of life, to bear witness and give voice to those who have had their rights stripped of them, silenced by locked doors, and told that their mistakes make them less than human."


Raising the Bar on Fundraising
------------------------------

Christopher Hartley, Deputy Dir. of UUA Stewardship and Development and Barry Finkelstein, Stewardship for Us

http://www.stewardshipforus.com


