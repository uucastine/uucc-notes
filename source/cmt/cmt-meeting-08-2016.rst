Collaborative Ministry Team August 2016 meeting
=================================================

Wednesday, August 17, 10 to 11:38 a.m.

1. Internship and salary 
2. Soul Matters; 
3. June, 2017 Joint Worship Service; 
4. Common Read; 
5. Consistent Meeting Times for CMT 
6. Membership of CMT

Attendence
----------

* Robin Lovrien [Ellsworth]
* Mary Haynes via Skype [Ellsworth]
* Karen Wigglesworth [Belfast]
* Colin Powell [Castine]
* Lane Fisher [Intern]
* Rev. Margaret Beckman [Castine]
* Rev. Deane Perkins [Belfast]
* Rev. Sara Hayman [Ellsworth]

Internship and salary update
----------------------------

Sara opened the discussion noting that the funding source being used for the internship will likely be less next year. While not an issue right now with Lane's internship, it means we'll have to consider finding more funding for next year.

Consensus was to trust that the money will come through our congegations and our funds. Stipend for the next year is a quarter of what it was this year. Mary Haynes feels the planning for a new intern next year would not be part of the intern stewardship committee.

Think the committee is formed for a particular intern, and we don't know who will want to continue on the committee next year.

First internship committee meeting last week. Six members, (five at the moment). Encouraging congregations to hold a reception for Lane in each community. Regular meetings are first Wednesday of the month in the evenings.

Sara informed the team of a suggestion from a collegue of having a formal covenantal start with the congregation. We could hold the start at the retreat in Oct. and record it for later viewing. 

Lane starts on September 1.

Soul Matters
------------

$600 annual subscription, and Sara recommends that as a collaborative we may consider applying together next year.

Sara handed out examples of the monthly themes Soul Matters provides. Noted that Ellsworth receieved a discount on this year's subscription, but wondered if we'd all like to get on board next year.

Idea was well recevied, will be taken back to respective boards.

June 2017 Joint Worship Service
-------------------------------

June 11, 2017 –> same week this past year. Sara sent an email to Kate Braestrup asking to be our guest worship leader. Hope to have it at the new santctuary in Belfast.

Might consider hiring a project manager for the next service.

Need to begin to put on paper in a policy/procedure way, who should be responsible for what.  
Collaborative Worship Arts committee organized most of the service. A lot of things happened outside of those meetings.

$700 raised for the Guatemala travel project.

Colin sidetracked meeting with discussion about the need for collaborative tools beyond email. He will do up a proposal on using Basecamp as that tool for September meeting.

Common Read
-----------

UU adult education with our three congregations, a common book. 3 books over the course of the year. Fall, winter, spring semesters. Could be the whole congregations, could be just the CMT.

Suggested:

* Fall - The Third Reconstruction
* Winter - The New Jim Crow
* Spring - The Indigenous People's History

Collaborative book discussions? Otherwise we're just doing the Common Read. 

Start out with just a book discussion, but plan a collaborative event to finish the book off in November.

Goal would be to bring topics of social justice and spirituality up in a safe context for people to discuss, and to perhaps bring the three congregations closer in collaborative small group discussions at the end of the Common Read semester

Consistent meeting time & membership of CMT
-------------------------------------------

Second Tuesday of every month from 1 - 2:30 p.m.

To be discussed at the next meeting is who the second member from Castine will be and whether minsiters need to be involved in every meeting.

Sara reminded us that with Leslie stepping down, Castine now only has one member on the CMT. Colin will bring up the issue with the Castine governing board on Thursday, August 18 and get back to the team with updates.

Announcements
-------------

Robin updated us on:

Lutheran Church in Ellsworth, 15th anniversary of 9/11 – > September 11, 7 p.m. Pickup choir at 6 p.m.

She asked that a representative from each congregation please do their best to attend.

Oct. 2, CROP Walk in Ellsworth
