Collaborative Ministry Team September 2016 meeting
=================================================

Wednesday, September 13, 2:32 p.m. @ UUCE

Agenda
------

1. Common Read kick off
2. Defying the Nazis community showing
3. Proposal for using Basecamp
4. Update on Soul Matters
5. Update on internship & compensation
6. Coordinator for join collaborative worship service
7. CMT MUUSAN involvement

Attendence
----------

* Robin Lovrien [Ellsworth]
* Mary Haynes via Skype [Ellsworth]
* Karen Wigglesworth [Belfast]
* Colin Powell [Castine]
* Rev. Deane Perkins [Belfast]
* Rev. Sara Hayman [Ellsworth]

Common Read kick off
--------------------

Notices went out in the last newsletters in the various churches. Also, Deane made an announcement at Sunday service last week in Belfast. Sara will catch up with Margaret about what she's doing to promote it in Castine. Mary suggested buying a kindle copy and tapping into it. Deane also proposed buying a half dozen for each congregation.

Discussion in indepenedent churches begin in November. Two months to read, and then Deane, Sara and Margaret will get together. GA Talk by Barber might also be made availalbe for folks to join the discussion with out being up on the reading. Sara also wondered about Lane being involved in the discussions in the various churches.

Community showing of Defying the Nazis
--------------------------------------
Discussed the idea of showing Defying the Nazis at the Bucksport Theater. Lynn Parsons orcestrated sponsoring Selma. Brook Minner is on the board. Robin will forward information to Colin. Shoot for early November.

Proposal for using Basecamp 
---------------------------

Colin did not have an update on this.

Update on Soul Matters
----------------------

Sara provided an overview and proposed the idea of doing something at Ferry Beach to let people become familiar with the program.

Update on internship & compensation
-----------------------------------

Deane and Sara met last week and are recommending the idea of reducing the stipend that our intern receives by $2,500 and holding it in reserve for planning for next year. Lane actually recommended it. We were given a $4,500 grant this year, and are allowed to apply for half of that next year. We also had a gift bringing the total up to $9,000. We will not get that gift again next year. The idea was to split the gift in half so we have it for next year.

Stipend was arrived at by a UUA guideline based on the size of our combined congregations. Mary said she wondered about what this means the future. The program wasn't just to have placements for our two seminarians, but to keep this program going.

Grant applications are due by November 1. Mary and Robin will coordinate making sure the application gets submitted.

As regards the Collaborative Intern Program ... vital we have an application and interview process.

Mary reported that the intern committee is working well with the first meeting with Lane already. Meetings are the first Monday of the month from 6 to 7:30 p.m.

Coordinator for joint collaborative worship service
---------------------------------------------------
Deane proposes that we hire a coordinator for the upcoming June service, $300 stipend or there about. Who it is, and how it works will have to be worked out.

A lot of work was done just to coordinate the work to get work done. We can probably be more efficiently organized. One or two people who might enjoy doing that sort of thing.

First coordinator could come up with an RFP or task description. The person hired will have to feel it out a bit. June 11 in Belfast is the current date and location.

Robin noted that the organization should be easier without trying to do it at Fort Knox.

Each congregation will come up with one or two members who might be up for being paid to coordinate the joint service.

CMT MUUSAN involvement
----------------------

Robin updated us that Saturday was the quarterly MUUSAN meeting. There are four bills being followed closely right now for November voting. Offer was made for chalice lighting specifically on those topics. A sermon and service provided by First Parish in Portland on climate change. A lot of MUUSAN resources but they need more members.

How can we join our yoked congregation team to propagate MUUSAN issues and help with fundraising?

MUUSAN table at the Common Ground Fair. Egg roll fundraiser weekend after next. Frequency of meetings

Adjournment 
