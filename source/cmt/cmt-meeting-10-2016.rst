Collaborative Ministry Team October 2016 meeting
=================================================

Wednesday, October 11, 1:15 p.m.

Agenda
------

1. Collaborative service organizer update
2. Internship & funding plans
3. Basecamp presentation
4. Defying the Nazi's showing update
5. Ferry Beach update
6. Common Read update

Attendence
----------

* Robin Lovrien [Ellsworth]
* Mary Haynes via Skype [Ellsworth]
* Karen Wigglesworth [Belfast]
* Colin Powell [Castine]
* Lane Fisher [Intern]
* Rev. Margaret Beckman [Castine]
* Rev. Sara Hayman [Ellsworth]

Collaborative service organizer update
---------------------------------------

Discussed the fact that no one had names as discussed last time.

Then discussed compensation and idea of paying $300, $100 per congregation. Lane asked if we had discussed whether the intern would be willing to take that on. Could even be a capstone project of an internship. Question now is whether the work would be over and above the usual work.

We also discussed the idea of having one person per congregation. Also discussed whether hiring someone to help with this might evolve to become an executive pastor to organize all sorts of things rather than the committee.

Second Sunday in June.

Last thought was to choose a host that rotates each year. They coordinate to plan the event.

Internship & funding updates
----------------------------

Mary updated the committee on the application for the stipend. The application doesn't require us to do anything except wait. But our plan to fund the intership next year needs to be discussed.

Lane updated the committee that the agreement form she has required $9,000 matching. Our removal of some of the funds is no longer matching. Sara and Deane have a meeting schedule to discuss that with the UUA.

We will delay discussion of how to handle payment until next month.

Basecamp presentation
---------------------

Basecamp: email aggregator, shared calendar, notifications

Defying the Nazis showing update
--------------------------------

Talked about doing a screening as a three-congregation + discussion event, but open it up to the public. Colin reported that the Alamo costs $225 to rent out for a showing.

Colin will follow up about doing a showing the week of the 14th of Nov. or Nov. 28. Brooke has resigned from being the Executive Dir. of the Alamo.

Ferry Beach update
------------------

Robin reported that she reviewed the Ferry Beach program last week.

Margaret noted that the Ellsworth Ferry Beach planning team tried to bring into some folks from other congregations, rather than recreating the committee as a collaborative initiative.

Discussed nature of planning these sorts of events, and the hope that going forward there's more collaboration.

Common Read update
------------------

Reading the Third Reformation with a discussion in Novemeber. Margaret brought up the idea of integrating Common Read with a telecommuting push.

Next meeting
------------

November 8, at 1 p.m. in Belfast

Adjourned at 2:42 p.m.
