.. GA 2016 Notes documentation master file, created by
   sphinx-quickstart on Wed Jun 22 10:52:13 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

UUCC Documents from Colin P
===========================

Contents:

.. toctree::
   :glob: 

   ga2016/*
   cmt/*
   minutes/*
   sj/*
   *



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

